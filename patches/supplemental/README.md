## DisTok CutTheCord: Supplemental Patch

This patch adds various helper functions that were previously part of slashcommands. It is required to have this patch if you're going to use patches such as tokenlogin.

![Captain's Log](https://elixi.re/i/ug70v29p.jpg)

#### Available and tested on:
- 9.9.1
- 9.9.2
- 9.9.3
- 9.9.4
- 9.9.6
- 10.0.6
- 10.0.7
- 10.1.1
- 10.1.2
- 10.1.3
- 10.1.5
- 10.1.6
- 10.1.9
- 10.2.0
- 10.2.1
- 10.2.2
- 10.2.3
- 10.2.4
- 10.2.5
- 10.2.6
- 10.2.9

