## DisTok CutTheCord: Litecord Patch

This patch replaces the gateway, cdn, invite and api endpoints with an example Litecord one.

Just replace all mentions of `dev.litecord.top` to your instance (smth like `sed -e 's/dev.litecord.top/your.instance.example/g' 843.patch > 843-custom.patch`).

Please keep in mind that litecord is free (gratis and libre) software, provided under a libre license. If you paid money for it, you got scammed.

#### Requires
- nozlib

#### Available and tested on:
- 8.3.2
- 8.3.3
- 8.3.4g
- 8.3.5g
- 8.3.6g
- 8.3.9g
- 8.4.1g
- 8.4.2g
- 8.4.3g
- 8.4.4g
- 8.4.5g
- 8.4.8
- 8.5.0
- 8.5.1
- 8.5.3
- 8.5.4
- 8.5.5
- 8.5.6
- 8.7.6
- 8.8.4
- 8.8.8
- 8.9.6
- 8.9.7
- 8.9.8
- 8.9.9
- 9.0.0
- 9.0.1
- 9.0.2
- 9.0.3
- 9.0.4
- 9.0.6
- 9.0.9
- 9.1.0
- 9.3.9
- 9.4.0
- 9.4.2-SA
- 9.4.3-SA
- 9.4.5
- 9.4.6
- 9.4.7
- 9.4.8
- 9.6.3
- 9.6.4
- 9.6.5
- 9.6.6
- 9.6.7
- 9.6.8
- 9.7.0
- 9.8.0
- 9.8.4
- 9.8.6
- 9.9.1
- 9.9.2
- 9.9.3
- 9.9.4
- 9.9.6
- 10.0.6
- 10.0.7
- 10.1.1
- 10.1.2
- 10.1.3
- 10.1.5
- 10.1.6
- 10.1.9
- 10.2.0
- 10.2.1
- 10.2.2
- 10.2.3
- 10.2.4
- 10.2.5
- 10.2.6
- 10.2.9

