## DisTok CutTheCord: Token Login Patch

This patch allows you to login with just a token.

On login screen, set email to anything that starts with `tokenlogin`, such as `tokenlogin@example.com`. Set password to token, press login. Close app, restart it, and you'll automatically get logged in with that token.

This patch relies on supplemental, so please use that patch too.

#### Available and tested on:
- 9.0.3
- 9.0.4
- 9.0.6
- 9.0.9
- 9.1.0
- 9.3.8-SAO-Heathcliff
- 9.3.9
- 9.4.0
- 9.4.2-SA
- 9.4.3-SA
- 9.4.5
- 9.4.6
- 9.4.7
- 9.4.8
- 9.6.3
- 9.6.4
- 9.6.5
- 9.6.6
- 9.6.7
- 9.6.8
- 9.7.0
- 9.8.0
- 9.8.4
- 9.8.6
- 9.9.1
- 9.9.2
- 9.9.3
- 9.9.4
- 9.9.6
- 10.0.6
- 10.0.7
- 10.1.1
- 10.1.2
- 10.1.3
- 10.1.5
- 10.1.6
- 10.1.9
- 10.2.0
- 10.2.1
- 10.2.2
- 10.2.3
- 10.2.4
- 10.2.5
- 10.2.6
- 10.2.9
