# cutthecord

Modular Client Mod for Discord's Android app.

**Latest supported Discord Android version:** 10.2.9 (1029), released on 2020-01-28.

New patch development will be done for the latest supported version.

![A CutTheCord screenshot](https://elixi.re/i/h3eirsy9.png)

Check out [README.md in patches folder to see what patches are available and what each of them do](patches/README.md)!

## Binaries (apk)

An F-Droid repo is available on https://fdroid.a3.pm/seabear/repo/?fingerprint=9DC9CB5FDD85D37121A5FEE99D24475F03FEA7F2EC25FB94DD51866D87933ED1

You can add that to your phone and get updates easily or just download directly from there. **Rooting is NOT needed, CutTheCord can be installed alongside official Discord and/or other CutTheCord branches.**

Feel free to ignore play protect, it's bullshit.

If you fail recaptcha, [follow this](https://gitdab.com/distok/cutthecord/issues/22#issuecomment-82) (run through adb).

**PSA: Please keep in mind that you may be unable to receive updates due to an F-Droid bug.**

~~If you're affected by this, here's a workaround: F-Droid -> Settings -> Repositories -> Seabear, tap Share on top bar, copy to clipboard, tap Delete (next to share), then tap Add Repository. It should already be filled in (from clipboard), so add it and you should be able to get updates after that.~~

If you're affected by this, ensure that your F-Droid version is 1.7 or higher. As of time of writing, F-Droid 1.7 is alpha, and can only be downloaded by enabling Unstable Updates through Settings -> Advanced Settings.

## Building

See [BUILDING.md](BUILDING.md).
