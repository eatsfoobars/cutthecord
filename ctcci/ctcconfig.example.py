import os

APKTOOL_BIN = "java -jar /home/ave/apktool-cli-all.jar"  # use latest

# Set to false if you're going to use a local state.json file
# state.json has information about the latest discord version
LOCAL_STATE = False
STATE_FILE = "/home/ave/distok/state.json"

# APKs must be placed under $DISTOK_FOLDER/android/$PACKAGE_ID-$VERSION_NUMBER.apk
# Example: /home/ave/distok/android/com.discord-909.apk
DISTOK_FOLDER = "/home/ave/distok"

# Set if F-Droid repo should be automatically updated or not
# If set to False, you can leave FDROID_FOLDER empty
DO_FDROID = True
FDROID_FOLDER = "/var/www/fdroid/seabear"

# This is where APKs will be placed
RESULT_FOLDER = FDROID_FOLDER + "/repo"

# Repo for CutTheCord (https://gitdab.com/distok/cutthecord)
DO_GITPULL = True
REPO_FOLDER = "/home/ave/distokrepos/cutthecord"

# If false, jarsigner will be used for signing the apk.
DO_APKSIGNER = True

# Keystore file, alias and pass. Required.
KEYSTORE_FILE = "/home/ave/oof.keystore"
KEYSTORE_ALIAS = "distok"
KEYSTORE_PASS = "redacted"

# Set this to the python version you want to use. Needs to be 3.6+.
PYTHON_BIN = "python3.6"

# Folder where the apk will be extracted to, patched in, and packed back on
# You're not recommended to touch WORK_APK_PATH.
WORK_FOLDER = "/tmp/cutthecord"
WORK_APK_PATH = os.path.join(WORK_FOLDER, "discord")

# Package ID for discord. You should probably not touch this.
PACKAGE_ID = "com.discord"

# Set to force if you want builds to be done
# even if there's already a build for this version
FORCE = False

# Custom ringtones, default = applied to all the ones not explicitly stated
RINGTONES = {"ave": "/home/ave/sans.mp3",
             "default": "/home/ave/removeskype.mp3"}

# Custom icons, default = applied to all the ones not explicitly stated
ICONS = {"ave": "/home/ave/ctclogoave.png",
         "default": "/home/ave/ctclogo.png"}

# Custom dynamic icons, default = applied to all the ones not explicitly stated
DYN_ICONS = {"ave": {"bg": "/home/ave/distokrepos/cutthecord/icons/bg.png",
                     "fg": "/home/ave/distokrepos/cutthecord/icons/fg.png"},
             "default": {"bg": "/home/ave/dcbg.png",
                         "fg": "/home/ave/dcfg.png"}}

# Custom app names, default = applied to all the ones not explicitly stated
# Needs to be one word, __ gets replaced with space
APP_NAMES = {"ave": "CutTheCord__Ave",
             "litecord": "Litecord",
             "test": "CTC__Testing",
             "default": "CutTheCord"}

# Custom fonts, default = applied to all the ones not explicitly stated
FONTS = {"ellie": {"whitney_bold.ttf": "/home/ave/fonts/GoogleSans-Bold.ttf",
                   "whitney_semibold.ttf": "/home/ave/fonts/GoogleSans-Medium.ttf",
                   "whitney_medium.ttf": "/home/ave/fonts/GoogleSans-Regular.ttf"},
        "dyslexic": {"whitney_bold.ttf": "/home/ave/fonts/OpenDyslexic3-Bold.ttf",
                   "whitney_semibold.ttf": "/home/ave/fonts/OpenDyslexic3-Bold.ttf",
                   "whitney_medium.ttf": "/home/ave/fonts/OpenDyslexic3-Regular.ttf"},
        "murm": {"whitney_bold.ttf": "/home/ave/fonts/comicbd.ttf",
                 "whitney_semibold.ttf": "/home/ave/fonts/comicbd.ttf",
                 "whitney_medium.ttf": "/home/ave/fonts/comic.ttf"},
        "ave": {"sourcecodepro_semibold.ttf": "/home/ave/fonts/comic.ttf"},
        "default": {}}
